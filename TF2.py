# This program gets the html code from the update page of Team Fortress 2.
# It then prints the date of the last update and today's date.

import requests
import datetime

# get html code as string
url = "https://www.teamfortress.com/?tab=updates"
req = requests.get(url, 'html.parser')

html_str = req.text

# get the text between "<h2>" and "</h2>", the date is there 
index_left = html_str.index("<h2>") + 4
index_right = html_str.index("</h2>") - 11

date_str = html_str[index_left:index_right]

# obtain today's date (MM DD, YYYY)
today = datetime.date.today()

today_date = today.strftime("%B %d, %Y")

# print everything
print("Latest TF2 Update: " + date_str)
print("Today is " + today_date)
